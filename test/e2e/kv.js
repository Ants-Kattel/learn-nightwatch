var config = require('../../nightwatch.conf.js');

module.exports = {
  'KV': function(browser) {
    browser
      .url('https://kv.ee')
      .waitForElementVisible('body')
      .assert.containsText("body", "AVALEHE KUULUTUSED")
      .saveScreenshot(config.imgpath(browser) + 'kv_home.png')
      .windowSize('current', 1000, 1000)
      .assert.containsText("body", "Müük")
      .saveScreenshot(config.imgpath(browser) + 'kv_home_big.png')
      .click('select[id="dt_select"] option[value="2"]')
      .saveScreenshot(config.imgpath(browser) + 'kv_select_üür.png')
      .useXpath()
      .click('//*[text()="Otsi"]')
      .useCss()
      .saveScreenshot(config.imgpath(browser) + 'kv_Otsi.png')
      .useXpath()
      .click('//*[text()="TÄPSEM OTSING"]')
      .pause(500)
      .click('//*[text()="Saun"]')
      .click('//*[text()="Rõdu"]')
      .click('//*[text()="Otsi"]')
      .useCss()
      .saveScreenshot(config.imgpath(browser) + 'kv_saun_rõdu.png')
      .click('select[id="orderby_web"] option[value="pdwl"]')
      .saveScreenshot(config.imgpath(browser) + 'kv_orderby_expensive.png')
      .useXpath()
      .click('//*[@id="res"]/table/tbody/tr[1]/td[2]/h2')
      .pause(1000)
      .useCss()
      .waitForElementVisible('body')
      .saveScreenshot(config.imgpath(browser) + 'kv_first_object.png')
      .end();
  }
};
